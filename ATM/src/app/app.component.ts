import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Route, Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ATM';
  
  constructor(private router: Router){}

  goToRoute(route: string = '/card'): void{
    this.router.navigateByUrl(route);
  }
}
