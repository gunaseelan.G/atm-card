import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardComponent } from './card/card.component';
import { AppComponent } from './app.component';
import { CardDetailComponent } from './card-detail/card-detail.component';
import { LoginComponent } from './login/login.component';
import { HistoryComponent } from './history/history.component';
const routes: Routes = [
  // {path:'',component:AppComponent},
  {path:'card',component:CardComponent},
  {path:'card-detail',component:CardDetailComponent},
  {path:'login',component:LoginComponent},
  {path:'history',component:HistoryComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
