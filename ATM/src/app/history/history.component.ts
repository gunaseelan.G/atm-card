import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent {
   deatials: any;
   constructor(private http: HttpClient) {

   }
   Event(){
    this.http
        .get('https://guna-3a04c-default-rtdb.firebaseio.com/data.json')
        .subscribe((respone:any) => {
          this.deatials=respone;
        });
   }
}
