import { NgModule, importProvidersFrom } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
// import { ServiceService } from './service.service';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSelectModule} from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import{ MatButtonModule} from '@angular/material/button';
import { CardComponent } from './card/card.component';
import { CardDetailComponent } from './card-detail/card-detail.component';
import { LoginComponent } from './login/login.component';
import { HistoryComponent } from './history/history.component'
import { CardGenerateService } from './services/card.generate.service';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    CardDetailComponent,
    LoginComponent,
    HistoryComponent,
    CardComponent,
    CardGenerateService
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,MatInputModule,BrowserAnimationsModule,
    AppRoutingModule,MatSelectModule,MatFormFieldModule,MatButtonModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
